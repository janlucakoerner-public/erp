package de.janlucakoerner.erp.login

import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PostMapping

@Controller
class LoginController {
    @GetMapping("/")
    fun showLoginForm(model: Model): String {
        model.addAttribute("loginDataset", LoginDataset("", ""))
        return "login"
    }
    @PostMapping("/")
    fun getLoginData(@ModelAttribute loginDataset: LoginDataset, model: Model): String {
        model.addAttribute("loginDataset", loginDataset)
        return if (loginDataset.password == "")
            "home"
        else
            "login"
    }
}