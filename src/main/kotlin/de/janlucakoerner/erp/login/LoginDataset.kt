package de.janlucakoerner.erp.login

import kotlinx.serialization.Serializable

@Serializable
data class LoginDataset(var username: String, var password: String)