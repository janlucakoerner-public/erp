package de.janlucakoerner.erp.login

import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File

class LoginList {
    companion object {
        const val PATH: String = "src/main/resources/data/login.json"
    }
    val jsonFormat = Json { prettyPrint = true }
    var list = listOf<LoginDataset>()
    fun encodeJson(path: String = PATH): String {
        val json = jsonFormat.encodeToString(list)
        File(path).bufferedWriter().use {
            writer -> writer.write(json)
        }
        return json
    }
    fun decodeJson(jsonData: String? = null, path: String = PATH) {
        if (jsonData == null) {
            var json: String?
            File(path).bufferedReader().use {
                reader -> json = reader.readLines().joinToString(separator = "\n")
            }
            list = jsonFormat.decodeFromString(json!!)
        } else {
            list = jsonFormat.decodeFromString(jsonData)
        }

    }
    fun contains(loginDataset: LoginDataset): Boolean {
        for (entry: LoginDataset in list) {
            if (entry.username == loginDataset.username &&
                entry.password == loginDataset.password) {
                return true
            }
        }
        return false
    }
}