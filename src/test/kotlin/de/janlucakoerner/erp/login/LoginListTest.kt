package de.janlucakoerner.erp.login

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class LoginListTest {
    private lateinit var loginList: LoginList
    private val testDataJson = """
        [
            {
                "username": "max mustermann",
                "password": "max12345"
            },
            {
                "username": "marlene musterfrau",
                "password": "marlene54321"
            }
        ]
    """.trimIndent()
    private val testData = listOf(
        LoginDataset(
            username = "max mustermann",
            password = "max12345"
        ),
        LoginDataset(
            username = "marlene musterfrau",
            password = "marlene54321"
        )
    )
    @BeforeEach
    fun resetLoginList() {
        loginList = LoginList()
    }
    @Test
    fun encodeTest() {
        decodeTest()
        val json = loginList.encodeJson()
        Assertions.assertEquals(
            testDataJson,
            json
        )
    }
    @Test
    fun decodeTest() {
        loginList.decodeJson(testDataJson)
        Assertions.assertTrue(
            loginList.contains(testData[0]) &&
            loginList.contains(testData[1])
        )
    }
}